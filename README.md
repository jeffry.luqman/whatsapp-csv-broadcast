# Whatsapp CSV Broadcast
> Broadcast whatsapp text, image or attachment using csv file

## Getting Started
1. Make sure you have [Git](https://git-scm.com/), [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Clone repositories and go to the directory
```bash
git clone https://gitlab.com/jeffry.luqman/whatsapp-csv-broadcast.git && cd whatsapp-csv-broadcast
```
3. Install your dependencies
```bash
npm install
```
3. Copy csv example file to the data directory
```bash
cp -R example data
```
4. Start
```bash
npm start
```
5. Scan qr code with your whatsapp
