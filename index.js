const sulla = require('sulla')
const neatCsv = require('neat-csv')
const fs = require('fs')
const os = require('os')
const delayPerChat = 3000

sulla.create('session', false, {
	headless: false
}).then((client) => start(client))

function start(client) {
	fs.readFile('data/data.csv', async (err, data) => {
		if (err) {
			console.error(err)
			return
		} else {
			rows = await neatCsv(data)
			rows.forEach((row, index) => {
				setTimeout(() => {
					sendWhatsapp(client, row)
				}, delayPerChat*index)
			})
		}
	})
}

function sendWhatsapp(client, row) {
	if (row.to!=undefined && row.text!=undefined) {
		if (row.type=='text') {
			console.log('sending chat to '+row.to+' as text')
			client.sendText(setContactId(row.to), setBody(row.text))
		} else if (row.type=='image' && row.filename!=undefined) {
			console.log('sending chat to '+row.to+' as image')
			client.sendImage(setContactId(row.to), 'data/'+row.filename, row.filename, setBody(row.text))
		} else if (row.type=='file' && row.filename!=undefined) {
			console.log('sending chat to '+row.to+' as attachment')
			client.sendFile(setContactId(row.to), 'data/'+row.filename, row.filename, setBody(row.text))
		} else {
			console.log('type not found')
			console.log(row)
		}
	} else {
		console.log('number not found')
		console.log(row)
	}
}

function setContactId(to) {
	if (to.charAt(0)=='0') {
		return to.replace('0','62')+'@c.us'
	} else if (to.charAt(0)=='+') {
		return to.replace('+','')+'@c.us'
	} else {
		return to+'@c.us'
	}
}

function setBody(text) {
	return text.replace(/\\n/g, os.EOL)
}
